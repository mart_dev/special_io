<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temperature extends Model
{
    protected $table = 'temperatures';

    protected $fillable = [];

    public function getFormattedTemperature()
    {
        return number_format($this->attributes['price'], 2);
    }
}
