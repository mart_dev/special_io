<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Special Input/Ouput</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Styles -->
    <style>

        .navbar {
            background: orange !important;
        }

        .navbar-brand {
            color: white !important;
            font-weight: bold;
        }

        h5 {
            margin: 0px !important;
        }

        .first_temperature {
            color: #ffc412;
        }

        .second_temperature {
            color: #e66f00;
        }

        .sub-temperature {
            color: grey;
        }
    </style>
</head>
<body>
<!-- As a heading -->
<nav class="navbar navbar-light bg-dark">
    <span class="navbar-brand mb-0 h1">Special Input/Output</span>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <canvas id="canvas" class="mt-5" width="100" height="50"></canvas>
        </div>
        <div class="col-md-3">
            <h5 class="mt-5">Temperatuur 1</h5>
            <small>Laatste meting</small>
            <div class="row">
                <div class="col-md-6">
                    <h1 class="first_temperature">{{$temperatures['first'][0]->temperature ?? '-'}} &#8451;</h1>
                </div>
                <div class="col-md-2">
                    {{Carbon\Carbon::parse($temperatures['first'][0]->created_at)->format('H:i') ?? '-'}}
                </div>

            </div>
            @foreach($temperatures['first'] as $temperature)
                @if ($loop->first) @continue @endif
                <div class="row sub-temperature">
                    <div class="col-md-6">
                        <h5>{{$temperature->temperature ?? '-'}} &#8451;</h5>
                    </div>
                    <div class="col-md-2">
                        {{Carbon\Carbon::parse($temperature->created_at)->format('H:i') ?? '-'}}
                    </div>

                </div>
            @endforeach
        </div>
        <div class="col-md-3">
            <h5 class="mt-5">Temperatuur 2</h5>
            <small>Laatste meting</small>
            <div class="row sub-temperature">
                <div class="col-md-6">
                    <h1 class="second_temperature">{{$temperatures['second'][0]->temperature}} &#8451;</h1>
                </div>
                <div class="col-md-2">
                    {{Carbon\Carbon::parse($temperatures['second'][0]->created_at)->format('H:i')}}
                </div>

            </div>
            @foreach($temperatures['second'] as $temperature)
                @if ($loop->first) @continue @endif
                <div class="row sub-temperature">
                    <div class="col-md-6">
                        <h5>{{$temperature->temperature}} &#8451;</h5>
                    </div>
                    <div class="col-md-2">
                        {{Carbon\Carbon::parse($temperature->created_at)->format('H:i')}}
                    </div>

                </div>
            @endforeach
        </div>
    </div>
</div>
<script>

    $(document).ready(function(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $.ajax({
            url: '/graph-data',
            type: 'GET',
            dataType: 'JSON',
            success: function (data) {
                console.log(data);

                var config = {
                    type: 'line',
                    data: {
                        labels: data['first']['dates'],
                        datasets: [{
                            label: 'Temperatuursensor 1',
                            backgroundColor: '#ffc412',
                            borderColor: '#ffc412',
                            data: data['first']['temperatures'],
                            fill: false,
                        }, {
                            label: 'Temperatuursensor 2',
                            fill: false,
                            backgroundColor: '#e66f00',
                            borderColor: '#e66f00',
                            data: data['second']['temperatures'],
                        }]
                    },
                    options: {
                        responsive: true,
                        // title: {
                        //     display: true,
                        //     text: 'Temperatuurmetingen'
                        // },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Tijdstip'
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Temperatuur (Celsius)'
                                }
                            }]
                        }
                    }
                };

                var ctx = document.getElementById('canvas').getContext('2d');
                window.myLine = new Chart(ctx, config);

            }
        });

    });



    // window.onload = function() {
    //
    // };

    window.setInterval(function(){
        console.log('test');
        /// call your function here
    }, 60 * 1000 * 5);


</script>
</body>
</html>
