<?php

namespace App\Http\Controllers;

use App\Temperature;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TemperatureController extends Controller
{
    public function index()
    {
        $temperatures = [];
        $temperatures['first'] = Temperature::where('sensor', 1)->take(10)->orderBy('created_at', 'desc')->get();
        $temperatures['second'] = Temperature::where('sensor', 2)->take(10)->orderBy('created_at', 'desc')->get();

//        dd($temperatures['second']);

//        foreach($temperatures['first'] as $temperature)
//        {
//            $date = Carbon::createFromFormat('Y-m-d H:i:s', $temperature->created_at, 'CEST');
//            $temperature->created_at = $date->setTimezone('Europe/Amsterdam');
//        }
//
//        foreach($temperatures['second'] as $temperature)
//        {
//            $date = Carbon::createFromFormat('Y-m-d H:i:s', $temperature->created_at, 'CEST');
//            $temperature->created_at = $date->setTimezone('Europe/Amsterdam');
//        }

//        dd($temperatures);

        $graph = [
          'first' => [
              'dates' => Temperature::where('sensor', 1)->whereDate('created_at', Carbon::today())->take(10)->orderBy('created_at', 'desc')->pluck('created_at'),
              'temperatures' => Temperature::where('sensor', 1)->whereDate('created_at', Carbon::today())->take(10)->orderBy('created_at', 'desc')->pluck('temperature'),
          ],
          'second'  => [
              'dates' => Temperature::where('sensor', 2)->whereDate('created_at', Carbon::today())->take(10)->orderBy('created_at', 'desc')->pluck('created_at'),
              'temperatures' => Temperature::where('sensor', 2)->whereDate('created_at', Carbon::today())->take(10)->orderBy('created_at', 'desc')->pluck('temperature'),
          ],
        ];

//        dd($graph);

        return view('index', ['temperatures' => $temperatures, 'graph' => $graph]);
    }

    public function graph()
    {
        $graph = [
            'first' => [
                'dates' => Temperature::where('sensor', 1)->whereDate('created_at', Carbon::today())->take(10)->orderBy('created_at', 'desc')->pluck('created_at'),
                'temperatures' => Temperature::where('sensor', 1)->whereDate('created_at', Carbon::today())->take(10)->orderBy('created_at', 'desc')->pluck('temperature'),
            ],
            'second'  => [
                'dates' => Temperature::where('sensor', 2)->whereDate('created_at', Carbon::today())->take(10)->orderBy('created_at', 'desc')->pluck('created_at'),
                'temperatures' => Temperature::where('sensor', 2)->whereDate('created_at', Carbon::today())->take(10)->orderBy('created_at', 'desc')->pluck('temperature'),
            ],
        ];

        foreach($graph['first']['dates']  as $index => $date)
        {
            $graph['first']['dates'][$index] = $date->format('H:i');
        }

        foreach($graph['second']['dates'] as $index => $date)
        {
            $graph['second']['dates'][$index] = $date->format('H:i');
        }

        return $graph;
    }
}
